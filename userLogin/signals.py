from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from myBackpack.models import BackUser


@receiver(post_save, sender=User)
def attach_BackUser(sender, **kwargs):
    if kwargs.get('created', False):
        BackUser.objects.get_or_create(userAttached=kwargs.get('instance'), memberType="Free")
