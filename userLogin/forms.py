from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, label="Username")
    password = forms.CharField(widget=forms.PasswordInput, label="Password")

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            current_field = self.fields[field]

            current_field.widget.attrs['class'] = 'form-control'
            current_field.widget.attrs['placeholder'] = current_field.label


class RegisterForm(forms.Form):
    username = forms.CharField(max_length=150, label="Username")
    password = forms.CharField(widget=forms.PasswordInput, label="Password")
    repeated_password = forms.CharField(widget=forms.PasswordInput, label="Repeat password")
    email = forms.EmailField(label="Email")

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()

        password = cleaned_data.get("password", "")
        repeated_password = cleaned_data.get("repeated_password", "")

        if len(password) < 6 or len(repeated_password) < 6:
            raise forms.ValidationError("The length of the password must be equal or greater than 6 characters")

        if password != repeated_password:
            raise forms.ValidationError("Passwords did not match")

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            current_field = self.fields[field]

            current_field.widget.attrs['class'] = 'form-control'
            current_field.widget.attrs['placeholder'] = current_field.label
