from __future__ import unicode_literals

from django.apps import AppConfig


class UserloginConfig(AppConfig):
    name = 'userLogin'

    def ready(self):
        import userLogin.signals
