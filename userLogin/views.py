from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from django.urls import reverse_lazy

from django.views.generic import FormView
from django.views.generic import RedirectView

from . import forms


class Login(FormView):
    form_class = forms.LoginForm
    template_name = "login.html"

    def get_success_url(self):
        url = self.request.GET.get('next', None)

        if url:
            return url

        else:
            return reverse_lazy('myBackpack:index')

    def form_valid(self, form):
        username = form.cleaned_data.get('username', None)
        password = form.cleaned_data.get('password', None)

        user = authenticate(username=username, password=password)

        if user is not None:
            login(self.request, user)

            return super(Login, self).form_valid(form)

        else:
            error = "Invalid username and / or password."
            form.add_error(None, error)

            return self.render_to_response(self.get_context_data(form=form, error=error))

    def get_context_data(self, **kwargs):
        context = super(Login, self).get_context_data(**kwargs)
        context['login'] = True

        return context


class Logout(RedirectView):
    url = reverse_lazy('myBackpack:index')

    def get(self, request, *args, **kwargs):
        logout(request)

        return super(Logout, self).get(request, *args, **kwargs)


class Register(FormView):
    form_class = forms.RegisterForm
    template_name = 'register.html'
    success_url = reverse_lazy('myBackpack:index')

    def form_valid(self, form):
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')

        user = User.objects.create_user(username, email, password)
        user.save()

        login(self.request, user)

        return super(Register, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(Register, self).get_context_data(**kwargs)
        context['register'] = True

        return context
