from django.test import TestCase

from django.urls import reverse_lazy

from django.contrib.auth.models import User


class TestLogin(TestCase):

    def setUp(self):
        User.objects.create_user(username='jacob', email='jacob123@company.com', password='jacobpass123')

        self.username = 'jacob'
        self.password = 'jacobpass123'

    def test_follow_url(self):
        test_url = reverse_lazy('myBackpack:create-backpack')

        response = self.client.get(test_url, follow=True)
        login_redirect = self.client.post(response.redirect_chain[0][0], {'username': self.username, 'password': self.password})

        self.assertRedirects(login_redirect, test_url)

    # def test_without_redirect(self):
    #     login_url = reverse_lazy('userLogin:login')
    #     expected_url = reverse_lazy('myBackpack:index')

    #     response = self.client.post(login_url, {'username': self.username, 'password': self.password}, follow=True)

    #     self.assertRedirects(response, expected_url)

    def test_invalid_login(self):
        login_url = reverse_lazy('userLogin:login')

        response = self.client.post(login_url, {'username': 'caca', 'password': 'seca'})

        self.assertFormError(response, 'form', None, "Invalid username and / or password.")
