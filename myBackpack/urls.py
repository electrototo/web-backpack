from django.conf.urls import url

from myBackpack import views
from myBackpack import apiviews

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='index'),

    url(r'^new/backpack/', views.NewBackpack.as_view(), name='create-backpack'),

    url(r'^new/note/(?P<backpack_id>\d+)/',
        views.NewNote.as_view(), name='create-note'),

    url(r'^list/own/backpacks/',
        views.ListMyBackpacks.as_view(), name='my-backpacks'),

    url(r'^list/backpack/(?P<backpack_id>\d+)/notes/',
        views.ListBackpackNotes.as_view(), name='my-notes'),

    url(r'^detail/note/(?P<note_id>\d+)/',
        views.DetailNote.as_view(), name='detail-note'),

    url(r'^delete/note/(?P<note_id>\d+)/',
        views.DeleteNote.as_view(), name='delete-note'),

    url(r'^delete/backpack/(?P<backpack_id>\d+)/',
        views.DeleteBackpack.as_view(), name='delete-backpack'),

    url(r'^edit/backpack/(?P<backpack_id>\d+)/',
        views.EditBackpack.as_view(), name='edit-backpack'),

    url(r'^edit/note/(?P<note_id>\d+)/',
        views.EditNote.as_view(), name='edit-note'),
]

urlpatterns += [
    url(r'api/detail/note/(?P<note_id>\d+)/',
        apiviews.APIDetailNote.as_view(), name='api-detail-note'),

    url(r'api/detail/backpack/(?P<backpack_id>\d+)/',
        apiviews.APIDetailBackpack.as_view(), name='api-detail-backpack'),

    url(r'api/list/backpack/(?P<backpack_id>\d+)/notes/',
        apiviews.APIListBackpackNotes.as_view(), name='api-list-backpack-notes'),

    url(r'api/create/note/$',
        apiviews.APICreateNote.as_view(), name='api-create-note'),

    url(r'api/create/backpack/$',
        apiviews.APICreateBackpack.as_view(), name='api-create-backpack'),

    url(r'api/delete/backpack/(?P<backpack_id>\d+)/$',
        apiviews.APIDeleteBackpack.as_view(), name='api-delete-backpack'),

    url(r'api/delete/note/(?P<note_id>\d+)/$',
        apiviews.APIDeleteNote.as_view(), name='api-delete-note'),
]
