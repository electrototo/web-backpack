from django.contrib import admin
from .models import BackUser, Backpack, Notes, Tags


@admin.register(BackUser)
class BackUserAdmin(admin.ModelAdmin):
    list_display = ('userAttached', 'memberType')


@admin.register(Backpack)
class BackpackAdmin(admin.ModelAdmin):
    pass


@admin.register(Notes)
class NotesAdmin(admin.ModelAdmin):
    pass


@admin.register(Tags)
class TagsAdmin(admin.ModelAdmin):
    pass
