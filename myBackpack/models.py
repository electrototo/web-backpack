from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class BackUser(models.Model):
    userAttached = models.OneToOneField(User, verbose_name="Username")
    memberType = models.CharField("Membership", max_length=50)

    def __str__(self):
        return self.userAttached.username


class Backpack(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)

    creationDate = models.DateTimeField(auto_now_add=True)
    public = models.BooleanField(default=False)

    owner = models.ForeignKey(BackUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Notes(models.Model):
    backpack = models.ForeignKey(Backpack, on_delete=models.CASCADE)

    title = models.CharField(max_length=100)
    content = models.TextField()

    creationDate = models.DateTimeField(auto_now_add=True)

    public = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def short_description(self):
        if len(self.content) > 50:
            return "{}...".format(self.content[:50])
        else:
            return self.content


class Tags(models.Model):
    tag = models.CharField(max_length=20)
    notes = models.ManyToManyField(Notes)

    def __str__(self):
        return self.tag
