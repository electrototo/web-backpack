from rest_framework import serializers

from myBackpack.models import Backpack
from myBackpack.models import Notes

from django.urls import reverse_lazy


class NotesSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='backpack.owner.userAttached.username')

    class Meta:
        model = Notes
        fields = ('id', 'backpack', 'owner', 'title', 'content', 'public', 'creationDate')


class BackpackSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.userAttached.username')
    note_count = serializers.ReadOnlyField(source='notes_set.count')

    class Meta:
        model = Backpack
        fields = ('id', 'owner', 'name', 'description', 'creationDate', 'note_count', 'public')


class BackpackNotesSerializer(serializers.ModelSerializer):
    notes = serializers.SerializerMethodField()
    owner = serializers.ReadOnlyField(source='owner.userAttached.username')

    class Meta:
        model = Backpack
        fields = ('id', 'owner', 'name', 'notes')

    def get_notes(self, obj):
        if self.context['request'].user != obj.owner.userAttached:
            notes = obj.notes_set.filter(public=True)
        else:
            notes = obj.notes_set.all()

        notes_urls = []

        for note in notes:
            notes_urls.append(reverse_lazy('myBackpack:api-detail-note',
                                           kwargs={'note_id': note.pk}))

        return notes_urls
