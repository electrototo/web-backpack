from django.test import TestCase
from django.urls import reverse_lazy

from django.contrib.auth.models import User

from myBackpack.models import Backpack
from myBackpack.models import BackUser


class TestNewBackpack(TestCase):
    def setUp(self):
        User.objects.create_user(username='jacob', password='jacob123')

    def test_call_view_denies_anonymous(self):
        """
        Test if the view redirects an anonymous user to login in
        before creating a new backpack
        """

        expected_url = '{}?next={}'.format(reverse_lazy('userLogin:login'), reverse_lazy('myBackpack:create-backpack'))

        # Get method
        response = self.client.get(reverse_lazy('myBackpack:create-backpack'), follow=False)
        self.assertRedirects(response, expected_url)

        # Post method
        response = self.client.post(reverse_lazy('myBackpack:create-backpack'), follow=False)
        self.assertRedirects(response, expected_url)

    def test_valid_form(self):
        """
        Test if the view redirects after a valid creation of a
        backpack
        """

        backpack_url = reverse_lazy('myBackpack:create-backpack')

        self.client.login(username='jacob', password='jacob123')
        response = self.client.post(backpack_url, {'name': 'New backpack', 'description': 'The description', 'public': 'on'})

        self.assertRedirects(response, reverse_lazy('myBackpack:my-backpacks'))


class TestPermissionsCreationAndDeletion(TestCase):
    def setUp(self):
        User.objects.create_user(username='jacob', password='jacob123')
        User.objects.create_user(username='Another', password='another124')

        self.username = 'jacob'
        self.password = 'jacob123'

        self.client.login(username=self.username, password=self.password)

        user = User.objects.get(pk=1)
        user_back = BackUser.objects.get(userAttached=user)

        new_backpack = Backpack(owner=user_back, name="My new backpack", description="This is the description")
        new_backpack.save()

        another_user = User.objects.get(pk=2)
        another_user_back = BackUser.objects.get(userAttached=another_user)

        another_backpack = Backpack(owner=another_user_back, name="another backpack", description="this is the backpack")
        another_backpack.save()

        self.backpack = new_backpack
        self.another_backpack = another_backpack

    def test_new_note_saved_correctly(self):
        view_url = reverse_lazy('myBackpack:create-note', kwargs={'backpack_id': self.backpack.pk})
        success_url = reverse_lazy('myBackpack:my-notes', kwargs={'backpack_id': self.backpack.pk})

        response = self.client.post(view_url, {'backpack': self.backpack.pk, 'title': 'My new note', 'content': 'Hey, its me', 'public': 'on'})

        self.assertRedirects(response, success_url)

        note = self.backpack.notes_set.get(pk=1)
        self.assertEqual(note.title, 'My new note')

    def test_new_note_insert_another_backpack(self):
        """
        This tests will see if a user is trying to insert a note
        into a backpack that he does not have ownership.
        The view must return a form error.
        """

        view_url = reverse_lazy('myBackpack:create-note', kwargs={'backpack_id': self.backpack.pk})

        response = self.client.post(view_url, {'backpack': self.another_backpack.pk, 'title': 'Injecting', 'content': 'a new note', 'public': 'on'})
        self.assertFormError(response, 'form', None, 'The note cannot be added to other user\'s backpack')

    def test_view_backpack_not_public(self):
        """
        This test will expect a permission denied if a user is trying to see a private
        backpack
        """

        backpack_notes = reverse_lazy('myBackpack:my-notes', kwargs={'backpack_id': self.another_backpack.pk})
        response = self.client.get(backpack_notes)

        self.assertEqual(response.status_code, 403)
