from django.urls import reverse_lazy

from django.http import HttpResponseRedirect

from django.forms import ModelChoiceField
from django.forms import HiddenInput

from django.views.generic import RedirectView
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic import DeleteView
from django.views.generic import UpdateView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import UserPassesTestMixin

from django.shortcuts import get_object_or_404

from myBackpack.models import Backpack
from myBackpack.models import BackUser
from myBackpack.models import Notes

from myBackpack.forms import BackpackForm
from myBackpack.forms import NotesForm


class Index(RedirectView):
    url = reverse_lazy('myBackpack:my-backpacks')


class NewBackpack(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('userLogin:login')
    model = Backpack
    form_class = BackpackForm

    template_name = "newBackpack.html"
    success_url = reverse_lazy('myBackpack:my-backpacks')

    def get_initial(self):
        self.initial = {'owner': self.request.user}

        return super(NewBackpack, self).get_initial()

    def form_valid(self, form):
        name = form.cleaned_data.get('name')
        description = form.cleaned_data.get('description')
        public = form.cleaned_data.get('public')

        user_back = BackUser.objects.get(userAttached=self.request.user)

        self.object = Backpack(owner=user_back, name=name, description=description, public=public)
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(NewBackpack, self).get_context_data(**kwargs)
        context['new_backpack'] = True

        return context


class NewNote(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('userLogin:login')
    model = Notes
    form_class = NotesForm

    template_name = "new_note.html"

    def get_initial(self):
        backpack_to_add_note = self.kwargs.get("backpack_id")
        self.initial = {'backpack': backpack_to_add_note}

        return super(NewNote, self).get_initial()

    def form_valid(self, form):
        back_user = BackUser.objects.get(userAttached=self.request.user)

        form_backpack = form.cleaned_data.get('backpack')
        user_backpacks = Backpack.objects.filter(owner=back_user)

        if form_backpack not in user_backpacks:

            form.add_error(None, "The note cannot be added to other user's backpack")

            form.data = form.data.copy()
            form.fields['backpack'] = ModelChoiceField(queryset=Backpack.objects.filter(owner=back_user))

            return super(NewNote, self).form_invalid(form)

        self.success_url = reverse_lazy('myBackpack:my-notes', kwargs={'backpack_id': form_backpack.pk})

        return super(NewNote, self).form_valid(form)


class ListBackpackNotes(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('userLogin:login')

    context_object_name = "notes"
    model = Notes

    template_name = "list_self_notes.html"

    def get_queryset(self):
        back_user = BackUser.objects.get(userAttached=self.request.user)

        backpack = get_object_or_404(Backpack, pk=self.kwargs.get("backpack_id"))
        user_backpacks = Backpack.objects.filter(owner=back_user)

        if backpack not in user_backpacks:
            self.raise_exception = True
            self.permission_denied_message = "You don't have the permissions required to see the specified backpack."

            return super(ListBackpackNotes, self).handle_no_permission()

        else:
            self.queryset = backpack.notes_set.all()

            return super(ListBackpackNotes, self).get_queryset()

    def get_context_data(self, **kwargs):
        context = super(ListBackpackNotes, self).get_context_data(**kwargs)

        backpack = Backpack.objects.get(pk=self.kwargs.get("backpack_id"))

        context['backpack'] = backpack

        return context


class ListMyBackpacks(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('userLogin:login')

    model = Backpack
    context_object_name = "backpacks"

    template_name = "listBackpacks.html"

    def get_queryset(self):
        userBackpack = BackUser.objects.get(userAttached=self.request.user)
        self.queryset = Backpack.objects.filter(owner=userBackpack)

        return super(ListMyBackpacks, self).get_queryset()


class DetailNote(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    login_url = reverse_lazy('userLogin:login')

    model = Notes
    context_object_name = "note"
    template_name = "notes_detail.html"
    pk_url_kwarg = "note_id"

    raise_exception = True

    def test_func(self):
        note = self.get_object(queryset=None)

        return (note.backpack.owner.userAttached == self.request.user or
                note.public)


class DeleteNote(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    login_url = reverse_lazy('userLogin:login')

    model = Notes
    template_name = "delete_note.html"

    pk_url_kwarg = "note_id"

    raise_exception = True

    def post(self, request, *args, **kwargs):
        note = self.get_object()
        self.success_url = reverse_lazy('myBackpack:my-notes', kwargs={'backpack_id': note.backpack.pk})

        return super(DeleteNote, self).post(request, *args, **kwargs)

    def test_func(self):
        note = self.get_object()
        note_owner = note.backpack.owner.userAttached

        return note_owner == self.request.user


class DeleteBackpack(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    login_url = reverse_lazy('userLogin:login')

    model = Backpack
    template_name = "delete_backpack.html"

    pk_url_kwarg = "backpack_id"
    success_url = reverse_lazy('myBackpack:my-backpacks')

    raise_exception = True

    def test_func(self):
        backpack = self.get_object()

        return backpack.owner.userAttached == self.request.user


class EditNote(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    login_url = reverse_lazy('userLogin:login')

    model = Notes
    pk_url_kwarg = 'note_id'

    form_class = NotesForm

    template_name = 'edit_note.html'
    raise_exception = True

    def test_func(self):
        note = self.get_object()

        return note.backpack.owner.userAttached == self.request.user

    def get_form(self, form_class=None):
        form = super(EditNote, self).get_form(form_class=None)

        form.fields['backpack'].widget = HiddenInput()

        return form

    def get_success_url(self):
        note = self.get_object()

        self.success_url = reverse_lazy('myBackpack:my-notes', kwargs={'backpack_id': note.backpack.pk})

        return super(EditNote, self).get_success_url()

    def form_valid(self, form):
        form_backpack = form.cleaned_data['backpack']

        back_user = BackUser.objects.get(userAttached=self.request.user)

        if form_backpack in back_user.backpack_set.all():
            return super(EditNote, self).form_valid(form)
        else:
            form.add_error(None, "You don't have the permissions to save the no"
                           "te under another user backpack")

            note = self.get_object()

            form.data = form.data.copy()
            form.data['backpack'] = note.backpack.pk

            return super(EditNote, self).form_invalid(form)


class EditBackpack(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    login_url = reverse_lazy('userLogin:login')

    model = Backpack
    pk_url_kwarg = 'backpack_id'

    form_class = BackpackForm

    template_name = 'edit_backpack.html'

    success_url = reverse_lazy('myBackpack:my-backpacks')
    raise_exception = True

    def test_func(self):
        backpack = self.get_object()

        return backpack.owner.userAttached == self.request.user

    def get_initial(self):
        username = self.request.user
        self.initial = {'owner': username}

        return super(EditBackpack, self).get_initial()

    def form_valid(self, form):
        form_user = form.cleaned_data['owner']

        current_back_user = BackUser.objects.get(userAttached=self.request.user)

        if form_user == current_back_user:
            return super(EditBackpack, self).form_valid(form)
        else:
            form.add_error(None, "The Backpack that is being edited does not corresponds to the user's backpacks")

            form.data = form.data.copy()
            form.data['owner'] = self.request.user

            return super(EditBackpack, self).form_invalid(form)
