from rest_framework import permissions

from rest_framework.generics import RetrieveAPIView
from rest_framework.generics import CreateAPIView
from rest_framework.generics import DestroyAPIView

from rest_framework.exceptions import PermissionDenied
from rest_framework.status import HTTP_201_CREATED

from rest_framework.response import Response

from myBackpack.serializers import NotesSerializer
from myBackpack.serializers import BackpackSerializer
from myBackpack.serializers import BackpackNotesSerializer

from myBackpack.models import Backpack
from myBackpack.models import BackUser
from myBackpack.models import Notes

from myBackpack.permissions import BackpackPermission
from myBackpack.permissions import NotesPermission


class APIDetailNote(RetrieveAPIView):
    lookup_url_kwarg = "note_id"
    serializer_class = NotesSerializer

    queryset = Notes.objects.all()

    permission_classes = (permissions.IsAuthenticated, NotesPermission)


class APIDetailBackpack(RetrieveAPIView):
    lookup_url_kwarg = "backpack_id"
    serializer_class = BackpackSerializer

    queryset = Backpack.objects.all()

    permission_classes = (permissions.IsAuthenticated, BackpackPermission)


class APIListBackpackNotes(RetrieveAPIView):
    lookup_url_kwarg = "backpack_id"
    serializer_class = BackpackNotesSerializer

    queryset = Backpack.objects.all()

    permission_classes = (permissions.IsAuthenticated, BackpackPermission)


class APICreateNote(CreateAPIView):
    serializer_class = NotesSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        userNote = serializer.validated_data.get('backpack')
        userNote = userNote.owner.userAttached

        if userNote == request.user:
            return super(APICreateNote, self).create(request, *args, **kwargs)

        else:
            raise PermissionDenied


class APICreateBackpack(CreateAPIView):
    serializer_class = BackpackSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        name = serializer.validated_data.get('name')
        description = serializer.validated_data.get('description')

        userBack = BackUser.objects.get(userAttached=request.user)

        newBackpack = Backpack(owner=userBack, name=name, description=description)
        newBackpack.save()

        headers = self.get_success_headers(serializer.data)

        data = serializer.data
        data['id'] = newBackpack.pk

        return Response(data, status=HTTP_201_CREATED,
                        headers=headers)


class APIDeleteBackpack(DestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, BackpackPermission, )
    lookup_url_kwarg = 'backpack_id'

    queryset = Backpack.objects.all()


class APIDeleteNote(DestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, NotesPermission, )
    lookup_url_kwarg = 'note_id'

    queryset = Notes.objects.all()
