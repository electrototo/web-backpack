from django.forms import ModelForm
from django.forms import HiddenInput
from django.forms import IntegerField
from django.forms import BooleanField

from .models import Backpack
from .models import Notes


class BackpackForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BackpackForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            current_field = self.fields[field]

            if (current_field is not self.fields['public']):
                current_field.widget.attrs['class'] = "form-control"
                current_field.widget.attrs['placeholder'] = current_field.label

            elif current_field is self.fields['public']:
                current_field.widget.attrs['aria-label'] = "..."

    class Meta:
        model = Backpack
        fields = ['name', 'description', 'public', ]


class NotesForm(ModelForm):
    backpack = IntegerField(widget=HiddenInput, required=False)

    def clean_backpack(self):
        backpack_id = self.cleaned_data['backpack']

        print "backpack_id: %s" % backpack_id

        try:
            backpack_to_add_note = Backpack.objects.get(pk=backpack_id)
        except Backpack.DoesNotExist:
            backpack_to_add_note = None

        return backpack_to_add_note

    def __init__(self, *args, **kwargs):
        super(NotesForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            current_field = self.fields[field]

            if (current_field is not self.fields['backpack'] and
                    current_field is not self.fields['public']):

                current_field.widget.attrs['class'] = "form-control"
                current_field.widget.attrs['placeholder'] = self.fields[field].label

            elif current_field is BooleanField:
                current_field.widget.attrs['aria-label'] = "..."

    class Meta:
        model = Notes
        fields = ['backpack', 'title', 'content', 'public', ]
