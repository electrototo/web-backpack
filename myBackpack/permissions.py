from rest_framework import permissions


class BackpackPermission(permissions.BasePermission):
    """
    Check if the logged in user is the same as
    the backpack owner, or if the backpack is
    public.
    """

    def has_object_permission(self, request, view, obj):

        if request.method in permissions.SAFE_METHODS:
            return (obj.owner.userAttached == request.user or
                    obj.public)

        else:
            return obj.owner.userAttached == request.user


class NotesPermission(permissions.BasePermission):
    """
    Check if the logged in user has permissions
    to modify and see the details of the note
    """

    def has_object_permission(self, request, view, obj):

        if request.method in permissions.SAFE_METHODS:
            return (obj.backpack.owner.userAttached == request.user or
                    obj.public)

        else:
            return obj.backpack.owner.userAttached == request.user
